module.exports = {
	root: true,
	env: {
		node: true,
	},
	plugins: ['vuetify'],
	extends: ['plugin:vue/essential', '@vue/prettier'],
	rules: {
		'no-console': 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		indent: [2, 'tab', { SwitchCase: 1 }],
		'no-tabs': 0,
		'max-len': ['error', { code: 500 }],
		'no-unused-vars': 0,
		'no-undef': 0,
		'vuetify/no-deprecated-classes': 'error',
		'vuetify/grid-unknown-attributes': 'error',
		'vuetify/no-legacy-grid': 'error',
	},
	parserOptions: {
		parser: 'babel-eslint',
	},
}
